package main

import (
	"os"
	"path/filepath"
	"testing"

	"code.gitea.io/sdk/gitea"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/matryer/is"
)

func TestDGMR(t *testing.T) {
	token := os.Getenv("DGMR_TOKEN")
	if token == "" {
		t.Skip("skipping integration (no DGMR_TOKEN found)")
	}
	assert := is.New(t)

	tmp := t.TempDir()

	file1Path := filepath.Join(tmp, "file1.txt")
	file1, err := os.Create(file1Path)
	assert.NoErr(err) // Should create file 1
	file1.WriteString("file1")
	assert.NoErr(file1.Close()) // Should close file 1

	file2Path := filepath.Join(tmp, "file2.txt")
	file2, err := os.Create(file2Path)
	assert.NoErr(err) // Should create file 1
	file2.WriteString("file2")
	assert.NoErr(file2.Close()) // Should close file 1

	client, err := gitea.NewClient("https://gitea.com", gitea.SetToken(token))
	assert.NoErr(err) // Client should be created

	user, _, err := client.GetMyUserInfo()
	assert.NoErr(err) // Should get my user info

	repo, _, err := client.CreateRepo(gitea.CreateRepoOption{
		Name:          "dgmr-test",
		DefaultBranch: "main",
		Readme:        "Default",
		AutoInit:      true,
		Private:       true,
	})
	assert.NoErr(err) // Should create repo
	defer func() {
		_, err := client.DeleteRepo(user.UserName, repo.Name)
		assert.NoErr(err) // Should delete repo
	}()

	err = dgmr("https://gitea.com", user.UserName, repo.Name, "latest", token, []string{file1Path, file2Path}, false)
	assert.NoErr(err) // Should be able to create inital latest release

	commit, _, err := client.CreateFile(user.UserName, repo.Name, "test.txt", gitea.CreateFileOptions{
		Content: "test",
	})
	assert.NoErr(err) // Should create file

	clonePath := filepath.Join(tmp, "repo")
	_, err = git.PlainClone(clonePath, true, &git.CloneOptions{
		URL: repo.CloneURL,
		Auth: &http.BasicAuth{
			Username: user.UserName,
			Password: token,
		},
	})
	assert.NoErr(err)                 // Should clone repo
	assert.NoErr(os.Chdir(clonePath)) // Should chdir

	err = dgmr("https://gitea.com", user.UserName, repo.Name, "latest", token, []string{file1Path, file2Path}, false)
	assert.NoErr(err) // Should be able to create new latest release

	rel, _, err := client.GetReleaseByTag(user.UserName, repo.Name, "latest")
	assert.NoErr(err) // Should get latest release

	assert.Equal(commit.Commit.SHA, rel.Target) // Commit SHAs should be equal
	assert.Equal(len(rel.Attachments), 2)       // Should have 2 attachments
	assert.Equal(rel.IsDraft, false)            // Should not be a draft
}
