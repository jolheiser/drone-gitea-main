FROM golang:1.17-alpine as build

RUN apk --no-cache add build-base

WORKDIR /app

COPY . .

RUN make build

FROM alpine:latest
LABEL maintainer="john.olheiser@gmail.com"

COPY --from=build /app/drone-gitea-main /drone-gitea-main

ENTRYPOINT ["/drone-gitea-main"]