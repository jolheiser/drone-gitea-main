package main

import (
	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
)

var Version = "develop"

func main() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	zerolog.SetGlobalLevel(zerolog.InfoLevel)

	app := cli.NewApp()
	app.Name = "Drone Gitea Main Releaser"
	app.Description = "Drone plugin to re-publish main release"
	app.Version = Version
	app.Action = doAction

	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Turn on debug mode",
			EnvVars: []string{"PLUGIN_DEBUG"},
		},
		&cli.StringFlag{
			Name:    "base",
			Usage:   "Gitea base URL",
			EnvVars: []string{"PLUGIN_BASE"},
			Value:   "https://gitea.com",
		},
		&cli.StringFlag{
			Name:    "token",
			Usage:   "Gitea API token",
			EnvVars: []string{"PLUGIN_TOKEN"},
		},
		&cli.StringFlag{
			Name:    "tag",
			Usage:   "Tag for release",
			EnvVars: []string{"PLUGIN_TAG"},
			Value:   "latest",
		},
		&cli.StringFlag{
			Name:    "owner",
			Usage:   "Gitea owner",
			EnvVars: []string{"DRONE_REPO_OWNER", "PLUGIN_OWNER"},
		},
		&cli.StringFlag{
			Name:    "repo",
			Usage:   "Gitea repo",
			EnvVars: []string{"DRONE_REPO_NAME", "PLUGIN_REPO"},
		},
		&cli.StringSliceFlag{
			Name:    "files",
			Usage:   "Files to attach",
			EnvVars: []string{"PLUGIN_FILES"},
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal().Err(err).Msg("")
	}

}
