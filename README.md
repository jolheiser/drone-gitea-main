# Drone Gitea Main Releaser

## Settings

Required
* `base` - Base Gitea URL
* `token` - Gitea API token
* `files` - A list of file globs to attach to the release

Optional
* `owner` - Repository owner (default: `DRONE_REPO_OWNER`)
* `repo` - Repository name (default: `DRONE_REPO_NAME`)
* `tag` - Release tag name (default: "latest")
* `debug` - Debug mode
* `dry` - Don't actually delete/create release