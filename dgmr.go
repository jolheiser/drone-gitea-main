package main

import (
	"errors"
	"fmt"
	"os"
	"path"
	"path/filepath"
	"strings"

	"code.gitea.io/sdk/gitea"
	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/urfave/cli/v2"
)

func doAction(ctx *cli.Context) error {
	sanity(ctx)
	return dgmr(ctx.String("base"), ctx.String("owner"), ctx.String("repo"), ctx.String("tag"), ctx.String("token"), ctx.StringSlice("files"), ctx.Bool("debug"))
}

func dgmr(base, owner, repo, tag, token string, files []string, debug bool) error {
	if debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	}

	client, err := gitea.NewClient(base, gitea.SetToken(token))
	if err != nil {
		return err
	}

	r, _, err := client.GetRepo(owner, repo)
	if err != nil {
		return err
	}

	if strings.EqualFold(r.DefaultBranch, tag) {
		return errors.New("tag cannot match branch name")
	}

	oldRelease, resp, err := client.GetReleaseByTag(owner, repo, tag)
	if err != nil && resp.StatusCode != 404 {
		return err
	}

	if oldRelease != nil && oldRelease.ID > 0 {
		// Delete existing release
		log.Debug().Msgf("Deleting release #%d", oldRelease.ID)
		if _, err := client.DeleteRelease(owner, repo, oldRelease.ID); err != nil {
			return err
		}
		if err := deleteTag(tag, owner, token); err != nil {
			return err
		}
	}

	branch, _, err := client.GetRepoBranch(owner, repo, r.DefaultBranch)
	if err != nil {
		return err
	}

	// Create main release
	log.Debug().Msgf("Creating release %s", r.DefaultBranch)
	mainRelease, _, err := client.CreateRelease(owner, repo, gitea.CreateReleaseOption{
		Title:   r.DefaultBranch,
		TagName: tag,
		Target:  branch.Commit.ID,
	})
	if err != nil {
		return err
	}

	// Upload attachments
	uploads := make([]string, 0)
	for _, glob := range files {
		globbed, err := filepath.Glob(glob)
		if err != nil {
			return err
		}
		uploads = append(uploads, globbed...)
	}

	for _, upload := range uploads {
		log.Debug().Msgf("Adding %s to attachment", upload)
		fi, err := os.Open(upload)
		if err != nil {
			return err
		}

		_, _, err = client.CreateReleaseAttachment(owner, repo, mainRelease.ID, fi, path.Base(upload))
		if err != nil {
			return err
		}

		if err := fi.Close(); err != nil {
			return err
		}
	}

	return nil
}

func sanity(ctx *cli.Context) {
	exit := false
	if !ctx.IsSet("token") {
		exit = true
		log.Error().Msg("--token is required")
	}
	if !ctx.IsSet("owner") {
		exit = true
		log.Error().Msg("--owner is required")
	}
	if !ctx.IsSet("repo") {
		exit = true
		log.Error().Msg("--repo is required")
	}
	if !ctx.IsSet("files") {
		exit = true
		log.Error().Msg("--files is required")
	}
	if exit {
		os.Exit(1)
	}
}

func deleteTag(tag, owner, token string) error {
	repo, err := git.PlainOpen(".")
	if err != nil {
		return err
	}
	log.Debug().Msgf("Deleting tag ref %s", tag)
	return repo.Push(&git.PushOptions{
		Auth: &http.BasicAuth{
			Username: owner,
			Password: token,
		},
		RefSpecs: []config.RefSpec{
			config.RefSpec(fmt.Sprintf(":refs/tags/%s", tag)),
		},
	})
}
